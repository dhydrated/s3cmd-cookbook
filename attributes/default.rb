default["s3cmd"]["version"] = nil
default['s3cmd']['users'] = []
default["s3cmd"]["download"]["s3_bucket"] = ""
default["s3cmd"]["download"]["object"] = ""
default['s3cmd']["download"]['bucket_location'] = "US"
default['s3cmd']["download"]['access_key'] = ""
default['s3cmd']["download"]['secret_key'] = ""
# default["s3cmd"]["download"]["local_file"]["name"] = ""
# default["s3cmd"]["download"]["local_file"]["owner"] = ""
# default["s3cmd"]["download"]["local_file"]["group"] = ""
# default["s3cmd"]["download"]["local_file"]["mode"] = ""

default['s3cmd']["cron"]["name"] = "my_cron"
default['s3cmd']["cron"]["action"] = "create"
default['s3cmd']["cron"]["minute"] = "0"
default['s3cmd']["cron"]["hour"] = "0"
default['s3cmd']["cron"]["weekday"] = "1"
default['s3cmd']["cron"]["home"] = "/"
default['s3cmd']["cron"]["command"] = "echo 'hello'"

