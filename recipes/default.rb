package "s3cmd"

node['s3cmd']['users'].each do |user|

  log "setting up for #{user}"

  home = user == "root" ? "/root" : "/home/#{user}"

  log "setting up for #{home}"


  if File.exists? home
    log "creating #{home}/.s3cfg"

    file = "#{home}/.s3cfg"

    template file do
        source "s3cfg.erb"
        mode 0655
    end
  else
    warn %Q(Home folder "#{home}" doesn't exist)
  end
end

# s3cmd_file node["s3cmd"]["download"]["local_file"]["name"] do
#   action :download
#   bucket node["s3cmd"]["download"]["s3_bucket"]
#   object_name node["s3cmd"]["download"]["object"]
#   owner node["s3cmd"]["download"]["local_file"]["owner"]
#   group node["s3cmd"]["download"]["local_file"]["group"]
#   mode node["s3cmd"]["download"]["local_file"]["mode"]
#   force true
# end

execute node["s3cmd"]["cron"]["name"] do
   command node["s3cmd"]["cron"]["command"]
end

cron node["s3cmd"]["cron"]["name"] do
  action node["s3cmd"]["cron"]["action"]
  minute node["s3cmd"]["cron"]["minute"]
  hour node["s3cmd"]["cron"]["hour"]
  weekday node["s3cmd"]["cron"]["weekday"]
  user node["s3cmd"]["user"]
  home node["s3cmd"]["cron"]["home"]
  command node["s3cmd"]["cron"]["command"]
end
